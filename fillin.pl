% You can use this code to get your started with your fillin puzzle solver.
main(PuzzleFile, WordlistFile, SolutionFile) :-
	read_file(PuzzleFile, Puzzle),
	read_file(WordlistFile, Wordlist),
	valid_puzzle(Puzzle),
	solve_puzzle(Puzzle, Wordlist,Solved),
	print_puzzle(SolutionFile, Solved).

read_file(Filename, Content) :-
	open(Filename, read, Stream),
	read_lines(Stream, Content),
	close(Stream).

read_lines(Stream, Content) :-
	read_line(Stream, Line, Last),
	(   Last = true
	->  (   Line = []
	    ->  Content = []
	    ;   Content = [Line]
	    )
	;  Content = [Line|Content1],
	    read_lines(Stream, Content1)
	).

read_line(Stream, Line, Last) :-
	get_char(Stream, Char),
	(   Char = end_of_file
	->  Line = [],
	    Last = true
	; Char = '\n'
	->  Line = [],
	    Last = false
	;   Line = [Char|Line1],
	    read_line(Stream, Line1, Last)
	).

print_puzzle(SolutionFile, Puzzle) :-
	open(SolutionFile, write, Stream),
	maplist(print_row(Stream), Puzzle),
	close(Stream).

print_row(Stream, Row) :-
	maplist(put_puzzle_char(Stream), Row),
	nl(Stream).

put_puzzle_char(Stream, Char) :-
	(   var(Char)
	->  put_char(Stream, '_')
	;   put_char(Stream, Char)
	).

valid_puzzle([]).
valid_puzzle([Row|Rows]) :-
	maplist(same_length(Row), Rows).


% solve_puzzle(Puzzle0, WordList, Puzzle)
% should hold when Puzzle is a solved version of Puzzle0, with the
% empty slots filled in with words from WordList.  Puzzle0 and Puzzle
% should be lists of lists of characters (single-character atoms), one
% list per puzzle row.  WordList is also a list of lists of
% characters, one list per word.
%
% This code is obviously wrong: it just gives back the unfilled puzzle
% as result.  You will need to replace this with a working
% implementation.

solve_puzzle(Puzzle,WordList,Solved):- 
  initialize_matrix(Puzzle,LogicPuzzle),
  all_slots_in_puzzle(LogicPuzzle,[],HorizonalSlotList),
  my_transpose(LogicPuzzle,TransInitialPuzzle),
  all_slots_in_puzzle(TransInitialPuzzle,[],VerticalSlotList),  
  append(HorizonalSlotList,VerticalSlotList,AllSlots),
  remove_one_square_slot(AllSlots,FillteredSlots),
  delete(FillteredSlots,[],FS),
  solving_puzzle(FS,WordList,WordList),
  Solved = LogicPuzzle.



remove_one_square_slot([],[]).
remove_one_square_slot([H],[R]):-
	(
	length(H,L),
	L=1,
	remove_one_square_slot([],R),!
	);
	(
	length(H,L),
        L>1,
	R = H,
	remove_one_square_slot([],[]),!
	).
remove_one_square_slot([H|T],[R|Rs]):-
	(
	length(H,L),
	L=1,
	remove_one_square_slot(T,[R|Rs]),!
	);
	(
	length(H,L),
	R = H,
	remove_one_square_slot(T,Rs),!
	).

solving_puzzle(AllSlots,[],WordListUnchanged):-AllSlots\=[].
solving_puzzle(AllSlots,WordList,WordListUnchanged):-
	length(WordList,Wl),	
	filling_priority(WordList,AllSlots,PriorityLists),
	try_each_element(PriorityLists,Nth),
	Nth \= [],
	fill_nth_slot(WordList,AllSlots,Nth,RemainingWords,ResultSlots),
	get_fitting_words_number(RemainingWords,ResultSlots,FittingNumList),
	valid_fill(WordListUnchanged,FittingNumList,ResultSlots),	
	solving_puzzle(ResultSlots,RemainingWords,WordListUnchanged),!.


replace_puzzle_hash([],[]).
replace_puzzle_hash([H|T],[R|Rs]):-
	replace_row_hash(H,R),
	replace_puzzle_hash(T,Rs).	

replace_row_hash([],[]).
replace_row_hash([H|T],[R|Rs]):-
	(
	var(H),
	H = '#',
	R = '#',
	replace_row_hash(T,Rs),!
	);
	(
	nonvar(H),
	R=H,
	replace_row_hash(T,Rs),!
	).

initialize_matrix(Puzzle,Result):-
	maplist(initialize_row, Puzzle, Result).

initialize_row([],[]).
initialize_row([RowH|RowT],[ResultH|ResultT]):-
	(
	RowH = '_'
	->
	ResultH = X
	;
        ResultH = RowH
	),
	initialize_row(RowT,ResultT).



% Transpose Matrix.
my_transpose([], []).
my_transpose([F|Fs], Ts) :-
    my_transpose(F, [F|Fs], Ts).

my_transpose([], _, []).
my_transpose([_|Rs], Ms, [Ts|Tss]) :-
        lists_firsts_rests(Ms, Ts, Ms1),
        my_transpose(Rs, Ms1, Tss).

lists_firsts_rests([], [], []).
lists_firsts_rests([[F|Os]|Rest], [F|Fs], [Os|Oss]) :-
        lists_firsts_rests(Rest, Fs, Oss).

all_slots_in_puzzle([],Result,Result).
all_slots_in_puzzle([PuzzleH|PuzzleT],Temp,Result):-
	all_slots_in_row(PuzzleH,SlotList),
        append(Temp,SlotList,NewTemp),
	all_slots_in_puzzle(PuzzleT,NewTemp,Result).	


all_slots_in_row(List,Result):-
	bagof(Slot, get_slots_from_row(List,Slot), Result).

get_slots_from_row(List,Result):-
        % Slot is the word.
        (
 	 get_dash_number(List,0,DashNum),
	 append(Slot,[],List),
	
         Slot \= [],
         word_slot(Slot),
	 get_dash_number(Slot,0,SlotDashNum),	
	 SlotDashNum = DashNum,
         Result = Slot
        );
        % Slot begin with the head.
        (
	 get_dash_number(List,0,DashNum),
	 append(Slot,[#|Post],List),

	
         Slot \= [],
         word_slot(Slot),
	 get_dash_number(Post,0,PartPostDashNum),
	 PostDashNum is PartPostDashNum + 1,

	 PostDashNum = DashNum,
         Result = Slot
        );
        % Slot end with the last element.
        (get_dash_number(List,0,DashNum),
	 append(Prev,[#|Slot],List),
	 

         Slot \= [],
         word_slot(Slot),
	 get_dash_number(Prev,0,PartPrevDashNum),
	 PrevDashNum is PartPrevDashNum + 1,
	 PrevDashNum = DashNum,
         Result = Slot
        );
        % Slot in the middle.
        (
	 get_dash_number(List,0,DashNum),
	 append(Prefix,[#|Post],List),
         append(Prev,[#|Slot],Prefix),
         append(Slot,[#|Post],Suffix),

         Slot \= [],
         word_slot(Slot),
	 get_dash_number(Prefix,0,PrevDashNum),
	 get_dash_number(Post,0,PartPostDashNum),
	 TotalDashNum is PrevDashNum + PartPostDashNum + 1,
	 TotalDashNum = DashNum,
         Result = Slot
         ).

word_slot([]).
word_slot([H|T]):-
	(
 	var(H),
	word_slot(T)
	);
	(
	nonvar(H),
	H\='#',
	word_slot(T)
	).

collect_slots_in_row([],[],Remian).
collect_slots_in_row([RowH|RowT],[ResultH|ResultT],RemainRow):-
	(
	var(RowH),
	ResultH = RowH,
	collect_slots_in_row(RowT,ResultT,RowT)
	);
        (
	collect_slots_in_row([],[ResultH|ResultT],RowT)
	).

get_dash_number([],Counter,Counter).
get_dash_number([H|T],Counter,Result):-
	(
 	nonvar(H),
	H = '#',
	NewNum is Counter + 1,
	get_dash_number(T,NewNum,Result),!
	);
	(
	get_dash_number(T,Counter,Result),!
	).

% Result is a list, each of them represents how many words the slot fits.
get_fitting_words_number(WordList,[],[]).
get_fitting_words_number(WordList,[SlotsH|SlotsT],[ResultH|ResultT]):-
	fitting_words_num(WordList,SlotsH,0,ResultH),	
	get_fitting_words_number(WordList,SlotsT,ResultT).
	
fitting_words_num([],Slot,NewNum,NewNum).
fitting_words_num([WordListH|WordListT],Slot,Num,Result):-
	(
        copy_term(Slot,CopyTerm),
	CopyTerm = WordListH,
	NewNum is Num + 1,
	fitting_words_num(WordListT,Slot,NewNum,Result),!
	);
	(
	fitting_words_num(WordListT,Slot,Num,Result),!
	).

sort_list([],[]).
sort_list(List,[R|Rs]):-
	min_list(List,R),
	delete(List,R,NewList),
	sort_list(NewList,Rs).

sort_slots(_,[],Result,Result).
sort_slots(FittingNumList,[SortedH|SortedT],Temp,Result):-
	
        findall(X, nth1(X,FittingNumList,SortedH), List),
	
	append(Temp,List,NewTemp),
	sort_slots(FittingNumList,SortedT,NewTemp,Result).


filling_priority(WordList,Slots,Result):-
	get_fitting_words_number(WordList,Slots,FittingNumList),

	sort_list(FittingNumList,SortedList),

        sort_slots(FittingNumList,SortedList,[],Result).





valid_fill(WordList,[],[]).
valid_fill(WordList,[FittingNumListH|FT],[SlotsH|ST]):-


	(
	all_non_var(SlotsH),
	FittingNumListH >= 0,
	valid_fill(WordList,FT,ST)
	);
	(
	non_all_var(SlotsH),

	FittingNumListH > 0,
	valid_fill(WordList,FT,ST)
	);
	(
	all_var(SlotsH),
	FittingNumListH > 0,
	valid_fill(WordList,FT,ST)	
	).

	


fill_nth_slot(WordList,Slots,N,RemainingWords,ResultSlots):-
	nth1(N,Slots,Slot),
	try_each_element(WordList,Word),
	Word\=[],
	Slot = Word,
	ResultSlots = Slots,
	delete(WordList,Word,RemainingWords).
	
try_each_element([],[]).
try_each_element([H|L],X):-
        X = H;
        try_each_element(L,X).

all_var([]).
all_var([H|T]):-
	var(H),
	all_var(T).

non_all_var(List):-
	not(all_var(List)).

all_non_var(List):-	
	maplist(nonvar,List).









	

